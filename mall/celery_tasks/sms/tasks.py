#coding:utf8
from libs.yuntongxun.sms import CCP
from celery_tasks.main import app

# 我们的任务需要通过 celery的实例对象的 task装饰器 装饰
# name 参数是 任务的名字
@app.task(name='send_sms_code')
def send_sms_code(mobile,sms_code):

    ccp = CCP()
    ccp.send_template_sms(mobile,[sms_code,5],1)