#coding:utf8
# borker_url 是 以Redis为我们的中间人, 它是指向 redis的第14号数据库
broker_url = "redis://127.0.0.1/14"

# 任务执行 有返回结果, redis 15好库 记录我们的结果
result_backend = "redis://127.0.0.1/15"