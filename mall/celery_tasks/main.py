#coding:utf8

from celery import Celery

#进行Celery允许配置
# 为celery使用django配置文件进行设置
import os
if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'mall.settings'



# 创建celery对象
# Celery 的main参数 ,习惯上给他脚本路径名
app = Celery('celery_tasks')


# 需要创建 Broker, 创建broker之前,我们需要创建celery对象
# 我们是通过配置文件来加载broker

# config_from_object 的参数是 配置文件的路径
app.config_from_object('celery_tasks.config')

# 我们需要让 celery自动检测到我的任务
# 列表里的元素就是 任务的路径
app.autodiscover_tasks(['celery_tasks.sms','celery_tasks.email','celery_tasks.html'])


# worker

# celery -A celery的实例对象脚本路径 worker -l info

# celery -A celery_tasks.main worker -l info

