#coding:utf8
from haystack import indexes

from .models import SKU


class SKUIndex(indexes.SearchIndex, indexes.Indexable):
    """
    SKU索引数据模型类
    """

    # document=True,
    #需要有一个（也是唯一一个）字段 document=True

    #  use_template=True  采用模板的形式来设置 检索的字段

    #
    #  text 惯例名称
    # text就相当于字典中的索引
    text = indexes.CharField(document=True, use_template=True)

    #设置显示的字段,是和模型关联的
    id = indexes.IntegerField(model_attr='id')
    name = indexes.CharField(model_attr='name')
    price = indexes.CharField(model_attr='price')
    default_image_url = indexes.CharField(model_attr='default_image_url')
    comments = indexes.IntegerField(model_attr='comments')

    def get_model(self):
        """返回建立索引的模型类"""
        return SKU

    def index_queryset(self, using=None):
        """返回要建立索引的数据查询集"""
        return self.get_model().objects.filter(is_launched=True)