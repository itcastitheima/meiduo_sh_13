from django.shortcuts import render
from django.views import View
from goods.models import GoodsCategory,GoodsChannel
from contents.models import ContentCategory
from collections import OrderedDict
# Create your views here.

class IndexView(View):


    def get(self,request):


        #数据查询出来
        # 使用有序字典保存类别的顺序
        # categories = {
        #     1: { # 组1
        #         'channels': [{'id':, 'name':, 'url':},{}, {}...],
        #         'sub_cats': [{'id':, 'name':, 'sub_cats':[{},{}]}, {}, {}, ..]
        #     },
        #     2: { # 组2
        #
        #     }
        # }
        # 初始化存储容器
        categories = OrderedDict()
        # 获取一级分类
        channels = GoodsChannel.objects.order_by('group_id', 'sequence')

        # 对一级分类进行遍历
        for channel in channels:
            # 获取group_id
            group_id = channel.group_id
            # 判断group_id 是否在存储容器,如果不在就初始化
            if group_id not in categories:
                categories[group_id] = {
                    'channels': [],
                    'sub_cats': []
                }

            one = channel.category
            # 为channels填充数据
            categories[group_id]['channels'].append({
                'id': one.id,
                'name': one.name,
                'url': channel.url
            })
            # 为sub_cats填充数据
            for two in one.goodscategory_set.all():
                # 初始化 容器
                two.sub_cats = []
                # 遍历获取
                for three in two.goodscategory_set.all():
                    two.sub_cats.append(three)

                # 组织数据
                categories[group_id]['sub_cats'].append(two)

        # 广告和首页数据
        contents = {}
        content_categories = ContentCategory.objects.all()
        # content_categories = [{'name':xx , 'key': 'index_new'}, {}, {}]

        # {
        #    'index_new': [] ,
        #    'index_lbt': []
        # }
        for cat in content_categories:
            contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')

        #进行模板的渲染
        context = {
            'categories': categories,
            'contents': contents
        }

        return render(request,'index.html',context)

#APIView
#GenericAPIView
#ListAPIView,RetireveAPIView

from rest_framework.generics import ListAPIView,GenericAPIView
from users.serializers import SKUSerialzier
from goods.models import SKU
from rest_framework.response import Response

class HotSKUView(GenericAPIView):
    # 因为我们设置了分页类,默认所有的列表页面 都采用这个分页类
    # 之前的一些页面,例如 省市区数据 也分页了, 不需要分页,需要在制定的视图中 设置 pagination_class=None
    pagination_class = None

    serializer_class = SKUSerialzier

    def get_queryset(self):
        category_id = self.kwargs['category_id']
        return  SKU.objects.filter(category_id=category_id,is_launched=True).order_by('-sales')[:2]

    def get(self,request,category_id):

        # skus = SKU.objects.filter(category_id=category_id)
        skus = self.get_queryset()
        serializer = self.get_serializer(skus,many=True)
        return Response(serializer.data)


# class HotSKUView(ListAPIView):
#
#     """
#     GET     goods/categories/?cat=xxxx
#
#     GET     goods/categories/<category_id>\d/
#     1. 根据某一个分类获取商品数据
#     2. 获取模型数据,然后将模型转换为JSON
#     """
#
#     serializer_class = SKUSerialzier
#
#     # queryset = SKU.objects.filter(cagegory_id=xxx)
#
#     def get_queryset(self):
#         # 我们可以通过 视图的kwargs来获取url canshu
#
#         category_id = self.kwargs['category_id']
#         return  SKU.objects.filter(category_id=category_id,is_launched=True).order_by('-sales')[:2]

from rest_framework.filters import OrderingFilter
from rest_framework.pagination import LimitOffsetPagination,PageNumberPagination
class SKUListView(ListAPIView):

    """
    GET     goods/categories/category_id/skus/

    1. 根据传递的 分类获取 分类数据, 获取的是模型,需要将模型转换为JSON
    2. 根据排序选项,进行排序
    3. 实现分页
    """

    serializer_class = SKUSerialzier

    def get_queryset(self):

        category_id = self.kwargs['category_id']

        return SKU.objects.filter(category_id=category_id,is_launched=True)


    #排序
    filter_backends = [OrderingFilter]
    ordering_fields = ['create_time','price','sales']

    # from utils.pagination import CustomPageNumberPagination
    # pagination_class = CustomPageNumberPagination


from .serializers import SKUIndexSerializer
from drf_haystack.viewsets import HaystackViewSet

class SKUSearchViewSet(HaystackViewSet):
    """
    SKU搜索
    """
    index_models = [SKU]

    serializer_class = SKUIndexSerializer
