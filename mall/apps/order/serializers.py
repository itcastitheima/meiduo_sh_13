#coding:utf8
from rest_framework import serializers
from goods.models import SKU
from decimal import Decimal
from django_redis import get_redis_connection
from order.models import OrderGoods

class CartSKUSerializer(serializers.ModelSerializer):
    """
    购物车商品数据序列化器
    """
    count = serializers.IntegerField(label='数量')

    class Meta:
        model = SKU
        fields = ('id', 'name', 'default_image_url', 'price', 'count')


class OrderSettlementSerializer(serializers.Serializer):
    """
    订单结算数据序列化器
    """
    freight = serializers.DecimalField(label='运费', max_digits=10, decimal_places=2)
    # serializer = CartSKUSerializer(skus,many=True)
    skus = CartSKUSerializer(many=True)


from order.models import OrderInfo
from django.db import transaction
import time
class OrderCommitSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderInfo
        fields = ('order_id', 'address', 'pay_method')
        read_only_fields = ('order_id',)
        extra_kwargs = {
            'address': {
                'write_only': True,
                'required': True,
            },
            'pay_method': {
                'write_only': True,
                'required': True
            }
        }


    def create(self, validated_data):

        """
        # 1.获取user,地址和支付方式
        # 2.生成订单id
        # 3.订单价格,订单数量和运费
        # 4.状态
        # 5.生成订单
        # 6. 连接redis获取信息
        # 7. 根据勾选的记录获取数据(sku_id,count)
        # 8. 对字典进行遍历
        # 9. 遍历的时候将商品添加到商品订单中
        # 10.商品信息 库存和销量的变化
        # 11.更新订单的数量和价格信息
        """
        # 这里省略了很多增删改查

        # 1.获取user,地址和支付方式
        user = self.context['request'].user
        address = validated_data.get('address')
        pay_method = validated_data.get('pay_method')
        # 2.生成订单id
        # 年月日时分秒+xxxx
        from django.utils import timezone
        # Y,year m:月  d     H   M   S
        order_id = timezone.now().strftime('%Y%m%d%H%M%S%f') + ('%09d'%user.id)
        # 3.订单价格,订单数量和运费
        total_count = 0
        # Decimal :货币类型
        # 0.123456789
        # 100 分三期  100/3 = 33.33    33.33   33.33       33.34
        total_amount = Decimal('0.0')
        freight = Decimal('10.00')
        # 4.状态
        # 状态是根据支付的选项来决定的
        # if pay_method == 1:
        #     # 货到付款
        #     status = 2
        # else:
        #     # 支付宝
        #     status = 1

        if pay_method == OrderInfo.PAY_METHODS_ENUM['CASH']:
            status = OrderInfo.ORDER_STATUS_ENUM['UNSEND']
        else:
            status = OrderInfo.ORDER_STATUS_ENUM['UNPAID']

        with transaction.atomic():
            #对以下代码使用 事务
            try:
                # 创建事务的恢复(保存)点
                save_id = transaction.savepoint()

                # 5.生成订单
                order = OrderInfo.objects.create(
                    order_id=order_id,
                    user = user,
                    address=address,
                    total_count=total_count,
                    total_amount=total_amount,
                    freight=freight,
                    pay_method=pay_method,
                    status=status
                )

                # 6. 连接redis获取信息
                redis_conn = get_redis_connection('cart')
                # 7. 根据勾选的记录获取数据(sku_id,count)
                # cart = {sku_id:count}
                redis_cart = redis_conn.hgetall('cart_%s'%user.id)
                redis_selected_ids = redis_conn.smembers('cart_selected_%s'%user.id)

                cart = {}
                for sku_id in redis_selected_ids:
                    cart[int(sku_id)] = int(redis_cart[sku_id])

                # 8. 对字典进行遍历
                for sku_id,count in cart.items():
                    while True:
                        sku = SKU.objects.get(pk=sku_id)
                        # 需要对商品的个数进行判断
                        if sku.stock < count:
                            # 参数是 回滚点是哪里
                            transaction.savepoint_rollback(save_id)
                            raise serializers.ValidationError('库存不足')

                        # 10.商品信息 库存和销量的变化
                        time.sleep(5)

                        # sku.stock -= count
                        # sku.sales += count
                        # #保存商品数量
                        # sku.save()


                        #  1. 先记录之前的查询结果
                        orginal_stock = sku.stock               # 15                    15

                        new_stock = sku.stock - count
                        new_sales = sku.sales + count
                        # 2. 在更新数据前, 再次查询一次结果,
                        # 如果一致 则更新数据,如果不一致则不更新
                                                                # 甲 15   15-10 = 5       乙   5!=15

                        # select * from sku where id=xxx and stock=xxx
                        rect = SKU.objects.filter(pk=sku_id,stock=orginal_stock).update(stock=new_stock,sales=new_sales)

                        if rect  == 0:
                            # 下单失败
                            continue
                            # raise serializers.ValidationError('下单失败')

                        # 9. 遍历的时候将商品添加到商品订单中
                        OrderGoods.objects.create(
                            order=order,
                            sku=sku,
                            count=count,
                            price=sku.price
                        )

                        # 11.更新订单的数量和价格信息
                        order.total_count += count
                        order.total_amount += (count*sku.price)
                        break
                # 保存订单信息
                order.save()
            except Exception:
                transaction.savepoint_rollback(save_id)
                raise serializers.ValidationError('下单失败')

            transaction.savepoint_commit(save_id)

        # 12. 下单成功,清除redis数据(选中的数据)
        # 勾选的商品id,都删除了
        #redis_selected_ids
        # [1,2,3,4]
        redis_conn.hdel('cart_%s'%user.id,*redis_selected_ids)
        redis_conn.srem('cart_selected_%s'%user.id,*redis_selected_ids)


        return order