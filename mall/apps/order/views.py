from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from django_redis import get_redis_connection
from goods.models import SKU
from .serializers import CartSKUSerializer,OrderSettlementSerializer
from rest_framework.response import Response
from decimal import Decimal

# Create your views here.
#APIView
#GenericAPIView
# ListAPIView
class PlaceOrderView(APIView):
    """
    GET     /orders/placesorders/

    # 1. 此视图必须是登录用户才可以访问
    # 2. 我们需要获取redis的数据, 获取 选中商品(选中的列表)的数据 sku_id,count
    # 3. 根据id获取商品的详细信息(模型)
    # 4. 模型转换为JSON
    """

    # 1. 此视图必须是登录用户才可以访问
    permission_classes = [IsAuthenticated]

    def get(self,request):
        # 可以直接使用user,因为已经有用户权限的验证了
        user = request.user
        # 2. 我们需要获取redis的数据, 获取 选中商品(选中的列表)的数据 sku_id,count
        # 2.1 连接redis
        redis_conn = get_redis_connection('cart')

        # 2.2 获取数据
        # 获取的是 选中和未选中的所有商品信息
        # {sku_id:count}
        redis_cart = redis_conn.hgetall('cart_%s'%user.id)

        #选中商品的id
        # [1,2,3]
        redis_selected_ids = redis_conn.smembers('cart_selected_%s'%user.id)


        # 2.3 组成 选中商品的数据 cart={sku_id:count}
        cart = {}
        for sku_id in redis_selected_ids:
            cart[int(sku_id)]=int(redis_cart[sku_id])

        # 3. 根据id获取商品的详细信息(模型)
        ids = cart.keys()
        skus = SKU.objects.filter(pk__in=ids)
        # [SKU,SKU,SKU]
        # SKU没有用户选中的个数
        #cart={sku_id:count}
        for sku in skus:
            sku.count = cart[sku.id]

        # 运费
        freight  = Decimal('10.00')


        # 4. 模型转换为JSON

        # serializer = CartSKUSerializer(skus,many=True)
        #
        # return Response(serializer.data)

        serializer = OrderSettlementSerializer({
            'freight':freight,
            'skus':skus
        })


        return Response(serializer.data)


# APIView
# GenericAPIView
# CreateAPIView
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAuthenticated
from .serializers import OrderCommitSerializer

class OrderView(CreateAPIView):

    """
    POST        /orders/

    1. 这个接口只能是登录用户才可以访问
    2. 点击提交订单按钮的时候必须要把 用户信息(jwt-token),地址, 支付方式 ,至于商品信息,我们是自己通过redis来获取的
    3. 校验数据(校验省略)
    4. 数据入库
    """
    permission_classes = [IsAuthenticated]
    # permission_classes = (IsAuthenticated) 少逗号

    serializer_class = OrderCommitSerializer

    # def post(self,request):
    #
    #     serialzier = XXXX(data=request.data)
    #     serialzier.is_valid()
    #     serialzier.save()

    #insert into t_a(name) select name from t_a; 蠕虫复制




