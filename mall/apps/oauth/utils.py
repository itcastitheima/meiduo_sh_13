#coding:utf8
from django.conf import settings
from urllib.parse import urlencode,parse_qs
from urllib.request import urlopen
import json

"""
1. 我们把要抽取(第二次写的代码,部分代码实现了指定功能)的代码复制到一个函数中
2. 哪里有问题改哪里 ,缺少的变量定义为参数
3. 把原代码注释,用抽取的代码进行测试

"""
class QQOauthTool(object):


    def get_auth_url(self):

        # state = 'test'
        #登录成功回调的地址
        state = '/'
        # 1. 注意,url后边要添加一个 ?
        base_url = 'https://graph.qq.com/oauth2.0/authorize?'

        # 2.把参数放到字典中
        params = {
            'response_type': 'code',
            'client_id': settings.QQ_APP_ID,
            'redirect_uri': settings.QQ_REDIRECT_URL,
            'state': state
        }

        # 3. 我们可以将字典的参数 拼接到url中
        auth_url = base_url + urlencode(params)


        return auth_url


    def get_token_by_code(self,code):
        base_url = 'https://graph.qq.com/oauth2.0/token?'

        params = {
            'grant_type': 'authorization_code',
            'client_id': settings.QQ_APP_ID,
            'client_secret': settings.QQ_APP_KEY,
            'code': code,
            'redirect_uri': settings.QQ_REDIRECT_URL
        }

        url = base_url + urlencode(params)

        # 3. 我们只能通过程序自己请求获取数据

        response = urlopen(url)

        # 通过 response 的read() 方法读取数据,bytes
        data = response.read().decode()
        # 'access_token=C7299EDD52B07DC8B0B3DFF767909803&
        # expires_in=7776000&
        # refresh_token=CBCAC05971E8769268AC3DCFA5B72F6B'
        # print(data)
        # parse_qs 将查询字符串转换为字典
        dict = parse_qs(data)

        # print(dict)

        tokens = dict['access_token']

        token = tokens[0]


        return token


    def get_openid_by_token(self,token):
        """
        PC网站：https://graph.qq.com/oauth2.0/me

        请求参数请包含如下内容：
        参数	是否必须	含义
        access_token	必须	在Step1中获取到的access token。
        """

        base_url = 'https://graph.qq.com/oauth2.0/me?'

        params = {
           'access_token':token
        }

        url = base_url + urlencode(params)

        # 请求url来获取数据
        response = urlopen(url)

        data = response.read().decode()

        # print(data)
        #'callback( {"client_id":"101474184","openid":"A9A6E67745D4ACAD694E4041F8DBBD07"} );'

        try:
            dict = json.loads(data[10:-4])
        except Exception:
            raise Exception('数据获取失败')

        # print(dict)

        openid = dict.get('openid')

        if openid:
            return openid

        return None




