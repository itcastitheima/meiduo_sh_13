#coding:utf8
from rest_framework import serializers
from .models import OAuthQQUser
from django_redis import get_redis_connection
from users.models import User

# serializers.ModelSerializer
# serializers.Serializer
class QQOauthTokenSerializer(serializers.Serializer):
    """
     # 1. 点击保存的时候,需要把 手机号,密码,短信验证码,token(openid) 传递过来
     #2. 对数据进行校验
     # 3. 绑定数据 ,将用户的user信息和 openid绑定
    """

    access_token = serializers.CharField(label='操作token')
    # 输入的数据 满足 regex 的正则表达式 数据才是正确的
    mobile = serializers.RegexField(label='手机号', regex=r'^1[345789]\d{9}$')
    password = serializers.CharField(label='密码', max_length=20, min_length=8)
    sms_code = serializers.CharField(label='短信验证码', max_length=6, min_length=6)

    # 多个字段的校验
    def validate(self, attrs):


        # 1. 校验token (token有没有被破坏, token的有效期)
        access_token = attrs.get('access_token')

        openid = OAuthQQUser.openid_token(access_token)

        #openid 有为 None的时候,我们需要判断
        if openid is None:
            raise serializers.ValidationError('绑定数据失败')

        ######
        attrs['openid']=openid

        # 2. 验证短信验证码
        #2.1获取验证码
        mobile = attrs.get('mobile')
        sms_code = attrs.get('sms_code')
        #2.2获取redis的验证码
        redis_conn = get_redis_connection('code')
        redis_code = redis_conn.get('sms_%s'%mobile)
        if redis_code is None:
            raise serializers.ValidationError('过期了')
        #2.3比较
        if redis_code.decode() != sms_code:
            raise serializers.ValidationError('验证码不一致')



        #3.获取user信息
        #根据手机号 获取user信息
        # 如果用户之前注册过,肯定输入过手机号,
        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            #不存在
            #创建一个用户
            pass
        else:
            #存在
            #校验输入的密码是否正确
            if not user.check_password(attrs['password']):
                raise serializers.ValidationError('密码输入错误')

            attrs['user']=user

        return attrs

    # attrs 的值 验证没有问题就传递给了 validated_data
    # 所以我们 attrs 添加数据, 这个数据是可以传递给 validated_data

    def create(self, validated_data):

        user = validated_data.get('user')

        if user is None:
            #创建用户
            user = User.objects.create(
                username=validated_data.get('mobile'),
                password=validated_data.get('password'),
                mobile=validated_data.get('mobile')
            )
            #修改密码
            user.set_password(validated_data['password'])
            user.save()


        OAuthQQUser.objects.create(
            user=user,
            openid=validated_data.get('openid')
        )

        return user


