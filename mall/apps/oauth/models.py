from django.db import models
from utils.models import BaseModel
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,BadSignature,SignatureExpired,BadData
from django.conf import settings


class OAuthQQUser(BaseModel):
    """
    QQ登录用户数据
    """
    # 采用 子应用.模型类名 关联
    # User只会在本子应用中查询
    user = models.ForeignKey('users.User', on_delete=models.CASCADE, verbose_name='用户')
    openid = models.CharField(max_length=64, verbose_name='openid', db_index=True)

    class Meta:
        db_table = 'tb_oauth_qq'
        verbose_name = 'QQ登录用户数据'
        verbose_name_plural = verbose_name





    # def generic_token_by_openid(self,openid):
    @staticmethod
    def generic_token_by_openid(openid):

        # 1,创建序列化器
        serializer = Serializer(settings.SECRET_KEY,3600)

        #2. 将敏感数据处理
        token = serializer.dumps({'openid':openid})

        # 3. token是bytes类型
        return token.decode()


    @staticmethod
    def openid_token(token):

        #1. 创建序列化器
        serializer = Serializer(settings.SECRET_KEY, 3600)
        #2.解码
        #BadSignature:  token被篡改
        #SignatureExpired: token过期
        try:
            result = serializer.loads(token)
        except BadData:
            # 数据有异常,返回None
            return None
        else:
            return result.get('openid')

