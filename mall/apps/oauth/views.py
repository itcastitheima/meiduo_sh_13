from django.shortcuts import render
from django.conf import settings
from urllib.parse import urlencode,parse_qs
from urllib.request import urlopen
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .utils import QQOauthTool
from .models import OAuthQQUser
from rest_framework.generics import GenericAPIView
from .serializers import QQOauthTokenSerializer

# Create your views here.

#1. 我们点击qq的按钮的时候,后台(我们的服务器)会生成一个url,这个url是根据腾讯的API生成的


# 2. 当用户扫描二维码之后,会生成一个code ,我们需要获取这个code

#3. 通过code换去 token,这个token有一定的时效

# 4. 通过token换去openid,openid才是腾讯给用户的唯一标示,我们需要将这个唯一表示和 我们的用户信息绑定

"""
urllib.parse.urlencode(query)
将query字典转换为url路径中的查询字符串


urllib.parse.parse_qs(qs)
将qs查询字符串格式数据转换为python的字典


urllib.request.urlopen(url, data=None)
发送http请求，如果data为None，发送GET请求，如果data不为None，发送POST请求
返回response响应对象，可以通过read()读取响应体数据，需要注意读取出的响应体数据为bytes类型
"""

class QQOauthURLView(APIView):

    """

    GET     oauth/qq/statues/
    这个url是根据腾讯的API生成的

    """

    def get(self,request):

        """
        # 生成auth_url
        # https://graph.qq.com/oauth2.0/authorize
        # 请求参数请包含如下内容：
        # response_type   必须      授权类型，此值固定为“code”。
        # client_id       必须      申请QQ登录成功后，分配给应用的appid。
        # redirect_uri    必须      成功授权后的回调地址，必须是注册appid时填写的主域名下的地址，建议设置为网站首页或网站的用户中心。注意需要将url进行URLEncode。
        # state           必须      client端的状态值。用于第三方应用防止CSRF攻击，成功授权后回调时会原样带回。请务必严格按照流程检查用户与state参数状态的绑定。
        # scope           可选      scope=get_user_info
        """
        # state = 'test'
        # #1. 注意,url后边要添加一个 ?
        # base_url = 'https://graph.qq.com/oauth2.0/authorize?'
        #
        # #2.把参数放到字典中
        # params = {
        #     'response_type':'code',
        #     'client_id':settings.QQ_APP_ID,
        #     'redirect_uri':settings.QQ_REDIRECT_URL,
        #     'state':state
        # }
        #
        #
        # # 3. 我们可以将字典的参数 拼接到url中
        # auth_url = base_url + urlencode(params)

        qq = QQOauthTool()

        auth_url = qq.get_auth_url()

        print(auth_url)
        return Response({'auth_url':auth_url})


# APIView
# GenericAPIView
# CreateAPIView,ListAPIView

class QQOauthTokenView(GenericAPIView):

    """
    GET     /oauths/xxxx/code=xxxx
    前端会调用ajxa 把 code给返回回来
    """

    serializer_class = QQOauthTokenSerializer

    def get(self,request):

        # 1. 获取code

        code = request.query_params.get('code')

        if code is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        # 2. 拼接url

        # PC网站：https://graph.qq.com/oauth2.0/token
        # GET
        # grant_type      必须      授权类型，在本步骤中，此值为“authorization_code”。
        # client_id       必须      申请QQ登录成功后，分配给网站的appid。
        # client_secret   必须      申请QQ登录成功后，分配给网站的appkey。
        # code            必须      上一步返回的authorization
        # redirect_uri    必须      与上面一步中传入的redirect_uri保持一致。


        qq = QQOauthTool()

        token = qq.get_token_by_code(code)

        openid = qq.get_openid_by_token(token)
        # print(openid)
        # print(token)

        if openid is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)


        # 根据openid 去判断用户之前是否绑定过
        try:
            qquser = OAuthQQUser.objects.get(openid=openid)
        except OAuthQQUser.DoesNotExist:
            # 不存在此openid,说明 第一次绑定
            # 我们需要将openid 返回给页面,让用户绑定的时候 和 openid绑定

            # 我们将openid进行 itsdangeros的处理

            # qq.generic_token_by_openid()
            token = OAuthQQUser.generic_token_by_openid(openid)

            return Response({'access_token':token})

        else:
            # 什么时候走else, try条件满足的时候
            #已经绑定过
            # 直接登录
            user = qquser.user
            # 在生成用户模型之后,生成一个登录的token
            from rest_framework_jwt.settings import api_settings

            # 获取2个方法
            jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
            jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

            # 让这2个方法执行
            payload = jwt_payload_handler(user)
            token = jwt_encode_handler(payload)

            return Response({
                'user_id': user.id,
                'username': user.username,
                'token': token
            })


    def post(self,request):
        """
        # 1. 点击保存的时候,需要把 手机号,密码,短信验证码,token(openid) 传递过来
        #2. 对数据进行校验
        # 3. 绑定数据 ,将用户的user信息和 openid绑定
        """

        #1. 获取用户提交的数据,传递给序列化器,进行校验
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid()

        #2.保存
        user = serializer.save()


        # 3. 返回token 和 user_id,username
        # 在生成用户模型之后,生成一个登录的token
        from rest_framework_jwt.settings import api_settings

        # 获取2个方法
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        # 让这2个方法执行
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        return Response({
            'user_id':user.id,
            'usnerame':user.username,
            'token':token
        })


"""
1.点击绑定保存的时候,把 access_token(openid),mobile,
password,sms_code提交过来
2. 校验数据
3. 绑定数据 user 信息和 openid信息,入库

"""



############################################
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

# 1. 我们创建序列化器
#secret_key,  秘钥
# expires_in  有效期,单位是 秒数

serializer = Serializer('abc',300)

# 2. 组织敏感数据
dict = {
    'openid':'123abc'
}

#3.对敏感数据进行编码
token = serializer.dumps(dict)
#'eyJleHAiOjE1MzMxMTUxNTQsImlhdCI6MTUzMzExNDg1NCwiYWxnIjoiSFMyNTYifQ.
# eyJvcGVuaWQiOiIxMjNhYmMifQ.
# 9WKMth7jLRSFOilewwcKxc8hveYOWeINqFqTrFq9JLI'


#4. 对token进行校验
serializer.loads(token)



serializer = Serializer('abc',1)

# 2. 组织敏感数据
dict = {
    'openid':'123abc'
}

#3.对敏感数据进行编码
token = serializer.dumps(dict)
#'eyJleHAiOjE1MzMxMTUxNTQsImlhdCI6MTUzMzExNDg1NCwiYWxnIjoiSFMyNTYifQ.
# eyJvcGVuaWQiOiIxMjNhYmMifQ.
# 9WKMth7jLRSFOilewwcKxc8hveYOWeINqFqTrFq9JLI'


#4. 对token进行校验
serializer.loads(token)

