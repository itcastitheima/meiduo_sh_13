#coding:utf8
from django.conf.urls import url
from . import views

urlpatterns = [
    #   /oauth/qq/statues/
    url(r'^qq/statues/$',views.QQOauthURLView.as_view(),name='statues'),

    #oauth/qq/users/
    url(r'^qq/users/$',views.QQOauthTokenView.as_view()),
]