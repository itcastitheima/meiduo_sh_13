from django.shortcuts import render
from order.models import OrderInfo
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated
# from django.conf import settings
from mall import settings
from alipay import AliPay
from .models import Payment

# Create your views here.
# 1. 创建应用,生成appid 和网关
# 2. 需要生成2套 私钥和公钥
#       我们的工程需要生成一套 公钥和私钥,      把公钥给支付宝,自己保留私钥
#       支付宝需要生成一套  公钥和私钥        我们需要把支付宝的公钥下载(复制)下来


from rest_framework.views import APIView
class PaymentView(APIView):

    """
    GET     /pay/orders/order_id/
    # 1. 前段需要将订单id提交给我们
    # 2. 查询订单信息
    # 3. 创建aliapy对象
    # 4. 生成 order_string
    # 5. 生成url

    """

    permission_classes = [IsAuthenticated]

    def get(self,request,order_id):
        # 1. 前段需要将订单id提交给我们
        # 2. 查询订单信息
        try:
            # 我们可以通过order_id 查询商品信息,但是
            #我们为了查询的更准确,应该再添加几个条件, 未支付的订单
            # select * from xxx where id=xx and user=xx
            order = OrderInfo.objects.get(order_id=order_id,
                                          user=request.user,
                                          status=OrderInfo.ORDER_STATUS_ENUM['UNPAID'])
        except OrderInfo.DoesNotExist:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        # 3. 创建aliapy对象

        app_private_key_string = open(settings.APP_PRIVATE_KEY_PATH).read()
        alipay_public_key_string = open(settings.ALIPAY_PUBLIC_KEY_PATH).read()

        alipay = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url
            app_private_key_string=app_private_key_string,
            alipay_public_key_string=alipay_public_key_string,  # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
            sign_type="RSA2",  # RSA 或者 RSA2
            debug = settings.ALIPAY_DEBUG  # 默认False
        )

        # 4. 生成 order_string
        # 如果你是 Python 3的用户，使用默认的字符串即可
        subject = "测试订单"

        # 电脑网站支付，需要跳转到https://openapi.alipay.com/gateway.do? + order_string
        order_string = alipay.api_alipay_trade_page_pay(
            out_trade_no=order.order_id,
            total_amount=str(order.total_amount),
            subject=subject,
            return_url="http://www.meiduo.site:8080/pay_success.html",
            notify_url=None  # 可选, 不填则使用默认notify url
        )
        # 5. 生成url
        aliapy_url = settings.ALIPAY_URL + '?' + order_string

        return Response({'alipay_url':aliapy_url})


class PaymentStatuView(APIView):

    """
    # 1. 前端把请求参数给我们,我们通过 校验来判断是否支付成功
    # 2. 如果支付成功,我们需要将 支付宝的订单id和网站的交易id保存, 更新订单状态(未支付-->未发货)
    # 3. 如果失败,返回失败信息
    """

    def put(self,request):
        # 1. 前端把请求参数给我们,我们通过 校验来判断是否支付成功
        # dict转换为字典
        data = request.query_params.dict()
        signature = data.pop("sign")

        app_private_key_string = open(settings.APP_PRIVATE_KEY_PATH).read()
        alipay_public_key_string = open(settings.ALIPAY_PUBLIC_KEY_PATH).read()

        alipay = AliPay(
            appid=settings.ALIPAY_APPID,
            app_notify_url=None,  # 默认回调url
            app_private_key_string=app_private_key_string,
            alipay_public_key_string=alipay_public_key_string,  # 支付宝的公钥，验证支付宝回传消息使用，不是你自己的公钥,
            sign_type="RSA2",  # RSA 或者 RSA2
            debug=settings.ALIPAY_DEBUG  # 默认False
        )

        success = alipay.verify(data, signature)

        if success:
            #out_trade_no		订单支付时传入的商户订单号,和支付宝交易号不能同时为空。
            # trade_no	支付宝交易号，和商户订单号不能同时为空	2014112611001004680 073956707


            # 2. 如果支付成功,我们需要将 支付宝的订单id和网站的交易id保存,
            # 2.1 保存数据
            out_trade_no = data.get('out_trade_no')
            trade_no = data.get('trade_no')

            Payment.objects.create(
                order_id=out_trade_no,  # 或者是 根据 out_trade_no 查询订单对象
                trade_id=trade_no
            )
            #  更新订单状态(未支付-->未发货)
            # 2.2 更新状态
            OrderInfo.objects.filter(order_id=out_trade_no).update(status=OrderInfo.ORDER_STATUS_ENUM['UNSEND'])

            return Response({'trade_id':out_trade_no})
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)
            # 3. 如果失败,返回失败信息


