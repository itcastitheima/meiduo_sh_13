from django.shortcuts import render
from rest_framework.viewsets import ReadOnlyModelViewSet
from .models import Area
from .serializers import AreaSerializer,SubsAreaSerialzier
# Create your views here.
from rest_framework_extensions.cache.mixins import ListCacheResponseMixin,RetrieveCacheResponseMixin

from rest_framework_extensions.cache.mixins import CacheResponseMixin

class AreaView(CacheResponseMixin,ReadOnlyModelViewSet):

    """
    省份信息 select * from tb_areas where parent_id is null;

    市的信息:select * from tb_areas where parent_id = 130000;
    区县信息:select * from tb_areas where parent_id = 130600;


    list        GET     /areas/infos/       获取省份的url
    retrieve    GET     /areas/infos/pk/    获取市/区县的url
    """
    # 设置属性不能满足我们的条件
    # queryset = Area.objects.all()

    pagination_class = None

    def get_queryset(self):
        # 根据业务逻辑的不同,返回不同的数据
        if self.action == 'list':
            #获取省份信息
            return Area.objects.filter(parent__isnull=True)
        else:
            #市/区县信息
            # 传入all(),但是我们知道 retrieve 会根据传递的pk 来获取指定数据
            return Area.objects.all()

    # serializer_class =

    def get_serializer_class(self):
        if self.action == 'list':
            return AreaSerializer
        else:
            return SubsAreaSerialzier


# class ShengView():
#     def get(self,request):
#         pass
#
# class ShiQuxianView():
#     def get(self,request,pk):
#         pass

"""
docker run -dti --network=host --name storage
 -e TRACKER_SERVER=192.168.229.133:22122
  -v /var/fdfs/storage:/var/fdfs delron/fastdfs storage
"""