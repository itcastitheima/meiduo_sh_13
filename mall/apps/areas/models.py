from django.db import models

class Area(models.Model):
    """
    行政区划
    """
    name = models.CharField(max_length=20, verbose_name='名称')
    # parent  自关联, ForeignKey('self'
    # on_delete=models.SET_NULL 从表设置为null
    # related_name='subs'  1个省份信息有多个 市  1个市有多个区县
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='subs', null=True, blank=True, verbose_name='上级行政区划')

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '行政区划'
        verbose_name_plural = '行政区划'

    def __str__(self):
        return self.name

#市
# class Area(models.Model):
#     """
#     行政区划
#     """
#     name = models.CharField(max_length=20, verbose_name='名称')
#     # parent  自关联, ForeignKey('self'
#     # on_delete=models.SET_NULL 从表设置为null
#     # related_name='subs'  1个省份信息有多个 市  1个市有多个区县
#     subs = [Area,Area,Area,Area]



                # 书籍模型  1
# 反向查询: 语法 关联模型类名小写_set


#人物模型   n
# book