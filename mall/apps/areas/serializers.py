#coding:utf8
from rest_framework import serializers
from .models import Area

# 省份信息
#[{"id":110000,"name":"北京市"},{"id":120000,"name":"天津市"},{"id":130000,"name":"河北省"}

class AreaSerializer(serializers.ModelSerializer):

    class Meta:
        model = Area
        fields = ['id','name']

#市/区县信息
#"subs":[{"id":130100,"name":"石家庄市"},{"id":130200,"name":"唐山市"},{"id":130300,"name":"秦皇岛市"},{"id":130400,"name":"邯郸市"},{"id":130500,"name":"邢台市"},{"id":130600,"name":"保定市"},

class SubsAreaSerialzier(serializers.ModelSerializer):

    # areas = [Area,Area,Area,Area]
    # serializer = AreaSerializer(areas,many=True)
    subs = AreaSerializer(many=True,read_only=True)

    class Meta:
        model = Area
        fields = ['subs','id','name']