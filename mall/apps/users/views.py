from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView
from .serializers import RegisterCreateUser
# from .models import User

# from mall.apps.users.models import User #错误的
# from apps.users.models import User    #错误的

from users.models import User    #正确的
from rest_framework import status

# Create your views here.

# APIView
# GenericAPIView
# ListAPIView,CreateAPIView

class RegisterUsernameView(APIView):

    """
    GET     /users/usernames/(?P<username>\w{5,20})/count/

    GET     /users/?usernaqme=xxxx
    POST    /users/             body: username=xxxx
    """

    def get(self,request,username):

        # 查询数据库判断 用户名是否存在
        count = User.objects.filter(username=username).count()
        # count = 0   没有注册过
        #ｃｏｕｎｔ＞＝1   注册过

        context = {
            'count':count,
            'username':username
        }

        return Response(context)


# APIView
# GenericAPIView
# ListAPIView,CreateAPIView

# CreateAPIView 选择路由:
#       有数据校验,肯定要创建序列化器
#       数据要入库
class RegisterCreateUserView(CreateAPIView):

    """
    POST        /users/

    1. 点击注册按钮的时候 需要将 username,password,password2,mobie,
    sms_code ,allow 提交给后台
    2. 我们需要对前端提交的数据进行校验
    3. 校验没有问题就可以入库了
    """

    serializer_class = RegisterCreateUser



# APIView
# GenericAPIView
# ListAPIView,RetrieveAPIView
from rest_framework.generics import GenericAPIView,RetrieveAPIView
from rest_framework.permissions import IsAuthenticated
from .serializers import UserCenterInfoSerializer
class UserCenterInfoView(GenericAPIView):
    """
    1.必须是登录用户
    2.我们需要返回指定用户的信息
    3. 将用户的信息 返回JSON数据
    """

    permission_classes = [IsAuthenticated]

    serializer_class = UserCenterInfoSerializer


    def get(self,request):

        # 得不到指定用户
        user = request.user
        #返回JSON数据
        serializer = self.get_serializer(instance=user)

        return Response(serializer.data)

class UserCenterInfo2View(RetrieveAPIView):

    permission_classes = [IsAuthenticated]

    serializer_class = UserCenterInfoSerializer

    # queryset = User.objects.all()

    def get_object(self):

        return self.request.user


"""
1.当用户点击设置邮箱的时候,把邮箱信息添加进去,点击保存按钮
2.更新用户的信息,只是保存用户的邮箱,
3. 需要添加一个字段去记录邮箱的状态
4. 当用户保存邮箱的时候我们需要发送一封激活邮件
5. 当用户点击激活的时候,修改用户的邮箱激活状态
"""

# APIView
# GenericAPIView
# UpdateAPIView,CreateAPIView
from rest_framework.generics import UpdateAPIView
from .serializers import EmailUpdateSerializer

class EmailUpdateView(UpdateAPIView):

    """
    1.更新用户的信息,只是保存用户的邮箱,
    2. 当用户保存邮箱的时候我们需要发送一封激活邮件
    3. 当用户点击激活的时候,修改用户的邮箱激活状态
    """

    permission_classes = [IsAuthenticated]

    serializer_class = EmailUpdateSerializer

    def get_object(self):

        return self.request.user



class VerifyEmailView(APIView):


    """
    GET        /users/verifications/?token=xxxx

    # 1. 将token信息 返回给后台
    # 2. 获取token之后,对token进行验证
    # 3. 获取用户的id,根据id查询用户信息
    # 4. 修改用户的信息状态
    """

    def get(self,request):

        # 1. 获取token之后,对token进行验证
        token = request.query_params.get('token')
        # 2. 获取用户的id,根据id查询用户信息
        # 把用户信息给我
        user = User.check_token(token)
        # user = User.objects.get(id=id)
        # 3. 修改用户的信息状态
        if user is None:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            user.email_active=True
            user.save()

            return Response({'message':'ok'})

#APIView
#GenericAPVIew
#CreateAPIView
from .serializers import UserHistorySerializer,SKUSerialzier
from rest_framework.mixins import CreateModelMixin
from rest_framework.generics import CreateAPIView
from django_redis import get_redis_connection
from goods.models import SKU

class UserHistoryView(GenericAPIView):

    """
    POST    /users/histories/
    GET     /users/histories/
    1. 必须是登录用户才可以访问此接口
    2. 访问详情页面的时候,向这里发送请求, 请求信息包括 用户信息和 商品id
    3. 校验数据
    4. 保存数据到Redis中
    """

    permission_classes = [IsAuthenticated]

    serializer_class = UserHistorySerializer

    def post(self,request):

        #1.获取数据,将数据传递给序列化器,并且进行校验
        serializer = self.get_serializer(data=request.data,context={'request':request})
        serializer.is_valid(raise_exception=True)
        serializer.save()


        #保存代码写在这里

        return Response({'message':'ok'})



    def get(self,request):
        """
        # 1.必须是登录用户
        # 2. 我们需要连接redis获取数据,获取的只是id 信息
        # 3.根据id信息查询的模型数据
        # 4. 需要将模型转换为JSON
        """
        # 1.必须是登录用户
        # 2. 我们需要连接redis获取数据,获取的只是id 信息
        redis_conn = get_redis_connection('history')

        ids = redis_conn.lrange('history_%s'%request.user.id,0,5)
        # [1,2,3,4,5]

        # 3.根据id信息查询的模型数据
        skus = []
        for id in ids:
            sku = SKU.objects.get(pk=id)
            skus.append(sku)

        # [SKU,SKU,SKU]
        # 4. 需要将模型转换为JSON
        serializer = SKUSerialzier(skus,many=True)

        return Response(serializer.data)

from rest_framework import mixins
from rest_framework import status
from .serializers import AddressSerializer
from rest_framework.decorators import action
from .serializers import AddressTitleSerializer
from rest_framework.viewsets import GenericViewSet

class AddressViewSet(mixins.ListModelMixin,mixins.CreateModelMixin,mixins.UpdateModelMixin,GenericViewSet):
    """
    用户地址新增与修改
    list GET: /users/addresses/
    create POST: /users/addresses/
    destroy DELETE: /users/addresses/
    action PUT: /users/addresses/pk/status/
    action PUT: /users/addresses/pk/title/
    """

    #制定序列化器
    serializer_class = AddressSerializer
    #添加用户权限
    permission_classes = [IsAuthenticated]
    #由于用户的地址有存在删除的状态,所以我们需要对数据进行筛选
    def get_queryset(self):
        return self.request.user.addresses.filter(is_deleted=False)

    def create(self, request, *args, **kwargs):
        """
        保存用户地址数据
        """
        count = request.user.addresses.count()
        if count >= 20:
            return Response({'message':'保存地址数量已经达到上限'},status=status.HTTP_400_BAD_REQUEST)

        return super().create(request,*args,**kwargs)

    def list(self, request, *args, **kwargs):
        """
        获取用户地址列表
        """
        # 获取所有地址
        queryset = self.get_queryset()
        # 创建序列化器
        serializer = self.get_serializer(queryset, many=True)
        user = self.request.user
        # 响应
        return Response({
            'user_id': user.id,
            'default_address_id': user.default_address_id,
            'limit': 20,
            'addresses': serializer.data,
        })

    def destroy(self, request, *args, **kwargs):
        """
        处理删除
        """
        address = self.get_object()

        # 进行逻辑删除
        address.is_deleted = True
        address.save()

        return Response(status=status.HTTP_204_NO_CONTENT)



    @action(methods=['put'], detail=True)
    def title(self, request, pk=None, address_id=None):
        """
        修改标题
        """
        address = self.get_object()
        serializer = AddressTitleSerializer(instance=address, data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(serializer.data)

    @action(methods=['put'], detail=True)
    def status(self, request, pk=None, address_id=None):
        """
        设置默认地址
        """
        address = self.get_object()
        request.user.default_address = address
        request.user.save()
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)


from rest_framework_jwt.views import ObtainJSONWebToken
from carts.utils import merge_cookie_to_redis

class UserAuthorizationView(ObtainJSONWebToken):

    def post(self, request):
        # 调用jwt扩展的方法，对用户登录的数据进行验证
        response = super().post(request)

        # 如果用户登录成功，进行购物车数据合并
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            # 表示用户登录成功
            user = serializer.validated_data.get("user")
            # 合并购物车
            #merge_cart_cookie_to_redis(request, user, response)
            response = merge_cookie_to_redis(request, user, response)

        return response





