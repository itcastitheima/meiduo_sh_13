from django.db import models
from django.contrib.auth.models import AbstractUser
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer,BadData,SignatureExpired,BadSignature
from django.conf import settings

# Create your models here.
class User(AbstractUser):

    # 额外添加一个字段
    mobile = models.CharField(max_length=11,unique=True,verbose_name='手机号')

    email_active = models.BooleanField(default=False, verbose_name='邮箱验证状态')

    default_address = models.ForeignKey('Address', related_name='users', null=True, blank=True,
                                        on_delete=models.SET_NULL, verbose_name='默认地址')

    class Meta:
        db_table = 'tb_users'
        verbose_name = '用户'
        verbose_name_plural = verbose_name


    def generic_verify_url(self):

        # 生成激活的url

        serializer = Serializer(settings.SECRET_KEY,3600)

        # 对敏感数据进行处理
        # 传递一个id 理论上是够了,也可以多传递一些信息
        token = serializer.dumps({'id':self.id,'email':self.email})
        # token的bytes的类型
        url = 'http://www.meiduo.site:8080/success_verify_email.html?token=' + token.decode()

        return url

    # 对象方法 ,第一个参数是self
    # 类方法, 第一个参数是 cls
    # 静态方法,第一个参数没有要求

    @staticmethod
    def check_token(token):
        #1.创建序列化器
        serializer = Serializer(settings.SECRET_KEY, 3600)
        #2. loads
        try:
            result = serializer.loads(token)
        except BadData:
            return None
        else:
            id = result.get('id')
            email = result.get('email')
            try:
                # 传递多个参数查询  id=id,email=email
                # where id=xxx and email=xxx
                # 让我们的查询 更准确
                # 其实传一个id就够了
                user = User.objects.get(id=id,email=email)
            except User.DoesNotExist:
                return None
            else:
                return user


from utils.models import BaseModel

class Address(BaseModel):
    """
    用户地址
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='addresses', verbose_name='用户')
    title = models.CharField(max_length=20, verbose_name='地址名称')
    receiver = models.CharField(max_length=20, verbose_name='收货人')
    province = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='province_addresses', verbose_name='省')
    city = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='city_addresses', verbose_name='市')
    district = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='district_addresses', verbose_name='区')
    place = models.CharField(max_length=50, verbose_name='地址')
    mobile = models.CharField(max_length=11, verbose_name='手机')
    tel = models.CharField(max_length=20, null=True, blank=True, default='', verbose_name='固定电话')
    email = models.CharField(max_length=30, null=True, blank=True, default='', verbose_name='电子邮箱')
    is_deleted = models.BooleanField(default=False, verbose_name='逻辑删除')

    class Meta:
        db_table = 'tb_address'
        verbose_name = '用户地址'
        verbose_name_plural = verbose_name
        ordering = ['-update_time']

