#coding:utf8
from rest_framework import serializers
from .models import User
import re
from django_redis import get_redis_connection
from django.conf import settings


# serializers.ModelSerializer
# serializers.Serializer

# 数据要入库
# 也有关联的模型
class RegisterCreateUser(serializers.ModelSerializer):
    """
      1. 点击注册按钮的时候 需要将 username,password,password2,mobie,
    sms_code ,allow 提交给后台
    2. 我们需要对前端提交的数据进行校验
    3. 校验没有问题就可以入库了
    """

    # password2  sms_code ,allow
    # 这三个 数据 没有自动生成字段
    # 系统没有自动生成的字段,我们手动实现就可以

    # 密码2只能写入,不能 进行读取(序列化)
    password2 = serializers.CharField(label='确认密码',write_only=True,
                                      allow_null=False,allow_blank=False)

    sms_code = serializers.CharField(label='短信验证码',write_only=True,
                                     min_length=6,max_length=6)

    allow = serializers.CharField(label='是否同意',write_only=True,
                                  allow_null=False)

    token = serializers.CharField(label='jwt-token',read_only=True)

    # ModelSerialzier 的生成字段是根据 fields来自动生成的
    class Meta:
        model = User
        fields = ['id','username','password','mobile','sms_code','allow','password2','token']

        extra_kwargs = {
            'id':{'read_only':True},
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }


    # 单个字段校验
    #手机号,allow
    def validate_mobile(self,value):
        # 是否满足手机号的规则
        if not re.match(r'1[3-9]\d{9}',value):
            raise serializers.ValidationError('手机号不正确')


        return value

    def validate_allow(self,value):

        if value != 'true':
            raise serializers.ValidationError('您未同意协议')

        return value


    #多个字段校验
    # 密码,短信验证码
    def validate(self, attrs):

        #1. 判断密码
        password = attrs.get('password')
        password2 = attrs.get('password2')
        if password != password2:
            raise serializers.ValidationError('密码不一致')

        #2.判断短信是否正确

        sms_code = attrs.get('sms_code')
        mobile = attrs['mobile']
        # 2.1 连接redis
        redis_conn = get_redis_connection('code')

        # 2.2 获取 redis数据
        redis_code = redis_conn.get('sms_%s'%mobile)
        # 判断 redis_code 有没有过期
        if redis_code is None:
            raise serializers.ValidationError('验证码已过期')

        #2.3 比较, Redis的数据是bytes类型
        if redis_code.decode() != sms_code:
            raise serializers.ValidationError('短信验证码不一致')

        return attrs



    def create(self, validated_data):


        print(validated_data)


        # validated_data 多了三个字段,这三个字段如果不删除 ,影响数据入库
        del validated_data['sms_code']
        del validated_data['allow']
        del validated_data['password2']


        print(validated_data)

        # 数据入库
        # User.objects.create(**validated_data)
        #或者
        # validated_data 数据已经是正确的了,可以入库
        user = super().create(validated_data)

        # 修改用户的密码
        user.set_password(validated_data['password'])
        user.save()
        #保存

        # 在生成用户模型之后,生成一个登录的token
        from rest_framework_jwt.settings import api_settings

        #获取2个方法
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        #让这2个方法执行
        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)


        # 我们返回 user之后,系统会调用 序列化操作,
        # 将能够序列化的数据 返回给响应
        user.token = token

        return user


class UserCenterInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id','username','mobile','email','email_active']


class EmailUpdateSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ['id','email']




    def update(self, instance, validated_data):


        email = validated_data.get('email')

        instance.email = email
        instance.save()

        # 在保存的同时发送激活邮件
        from django.core.mail import send_mail
        #subject, message, from_email, recipient_list,html_message=None
        #subject: 主题
        subject = '美多商城激活邮件'

        #  message, 普通信息
        message = ''

        # from_email,  谁发送的邮件
        from_email = settings.EMAIL_FROM#'qi_rui_hua@163.com'

        # recipient_list, 收件人 []
        recipient_list = [email]

        # html_message=None
        #instance.id
        #instance.email
        url = instance.generic_verify_url()

        html_message = '<p>尊敬的用户您好！</p>' \
                       '<p>感谢您使用美多商城。</p>' \
                       '<p>您的邮箱为：%s 。请点击此链接激活您的邮箱：</p>' \
                       '<p><a href="%s">%s<a></p>' % (email, url, url)


        # send_mail(subject,
        #           message,
        #           from_email,
        #           recipient_list,
        #           html_message=html_message)

        from celery_tasks.email.tasks import send_verify_email

        send_verify_email.delay(subject,
                  message,
                  from_email,
                  recipient_list,
                  html_message=html_message)

        return instance

# 序列化器
# 序列化, 反序列化(校验和入库)
from goods.models import SKU
class UserHistorySerializer(serializers.Serializer):

    sku_id = serializers.CharField(label='商品id',required=True)


    # def validate(self, attrs):

    def validate_sku_id(self,value):

        #1.判断商品是否存在
        try:
            SKU.objects.get(pk=value)
        except SKU.DoesNoeExist:
            raise serializers.ValidationError('商品不存在')

        return value


    def create(self, validated_data):

        # 数据保存到redis中

        #保存用户记录的 key
        # history_user.id
        sku_id = validated_data.get('sku_id')

        user = self.context['request'].user

        #1. 连接redis
        redis_conn = get_redis_connection('history')

        #2. 如果有重复的数据我们需要先把重复的数据删除,再写入
        # 先把重复的商品id删除
        redis_conn.lrem('history_%s'%user.id,0,sku_id)

        redis_conn.lpush('history_%s'%user.id,sku_id)
        #3. 只保留5条记录
        redis_conn.ltrim('history_%s'%user.id,0,4)



        return validated_data


class SKUSerialzier(serializers.ModelSerializer):

    class Meta:
        model = SKU
        fields = ['id', 'name', 'price', 'default_image_url', 'comments']

from users.models import Address
class AddressSerializer(serializers.ModelSerializer):

    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)
    mobile = serializers.RegexField(label='手机号', regex=r'^1[3-9]\d{9}$')

    class Meta:
        model = Address
        exclude = ('user', 'is_deleted', 'create_time', 'update_time')


    def create(self, validated_data):
        #Address模型类中有user属性,将user对象添加到模型类的创建参数中
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)

class AddressTitleSerializer(serializers.ModelSerializer):
    """
    地址标题
    """
    class Meta:
        model = Address
        fields = ('title',)

