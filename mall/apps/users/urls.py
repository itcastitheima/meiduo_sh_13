#coding:utf8
from django.conf.urls import url
from . import views

from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    #/users/usernames/(?P<username>\w{5,20})/count/
    url(r'^usernames/(?P<username>\w{5,20})/count/$',views.RegisterUsernameView.as_view(),name='usernamecount'),

    url(r'^$',views.RegisterCreateUserView.as_view()),

    #url 登录
    # users/auths/
    # url(r'^auths/$',obtain_jwt_token),
    url(r'^auths/$',views.UserAuthorizationView.as_view()),


    #/users/infos/
    # url(r'^infos/$',views.UserCenterInfoView.as_view()),
    url(r'^infos/$',views.UserCenterInfo2View.as_view()),

    url(r'^emails/$',views.EmailUpdateView.as_view()),

    #/users/emails/verification/
    url(r'^emails/verification/$',views.VerifyEmailView.as_view()),

    #/users/browerhistories/
    url(r'^browerhistories/$',views.UserHistoryView.as_view(),name='history'),
]

from .views import AddressViewSet
from rest_framework.routers import DefaultRouter
router = DefaultRouter()
router.register(r'addresses',AddressViewSet,base_name='address')
urlpatterns += router.urls