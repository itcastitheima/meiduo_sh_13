from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .serializers import CartSerialzier,CartSKUSerializer
from django_redis import get_redis_connection
import base64
import pickle
from goods.models import SKU
from .serializers import CartDeleteSerailzier
from rest_framework import status

# Create your views here.

"""
    1.用户未登录 我们将 sku_id, count,selected 保存到cookie中
    2.登录用户 保存在 redis中

"""


class CartView(APIView):


    """
    POST    /cart/          把商品 的sku_id, count,selected提交过来
    """


    # 如果用户登录了 ,request.user 表示登录用户的信息
    # 如果用户没登录了 ,request.user 表示匿名用户

    # 如果用户伪造了token是不能够进入 post的方法,
    # 所以我们需要重写perform_authentication 方法,让判断用户的信息在用户提交数据之后
    def perform_authentication(self, request):
        pass

    def post(self,request):
        """
        # 1.  sku_id, count,selected提交过来,
        # 2. 校验数据
        # 3. 根据用户的登录状态来保存数据
        # 4. 登录用户保存在reids中
        # 5. 未登录用户保存在cookie中
        """



        # print(request.user)
        #因为我们重写perform_authentication 方法,它会在我们获取用户信息的时候验证用户的真实信息
        # 所以获取用户信息存在异常,我们需要捕获异常


        # 1.  sku_id, count,selected提交过来,
        # 2. 校验数据,并且获取 校验之后的数据
        serialzier = CartSerialzier(data=request.data)
        serialzier.is_valid(raise_exception=True)

        sku_id = serialzier.validated_data.get('sku_id')
        count = serialzier.data.get('count')
        selected = serialzier.data.get('selected')

        # 3. 根据用户的登录状态来保存数据
        #所以获取用户信息存在异常, 我们需要捕获异常
        try:
            user = request.user
        except Exception:
            user = None
        # request.user.is_authenticated
        # is_authenticated 判断用户是否登录
        if user is not None and user.is_authenticated:
            # 4. 登录用户保存在reids中
            # 4.1 连接redis
            redis_conn = get_redis_connection('cart')

            # 4.2 把数据保存到redis中
            # hash
            # redis_conn.hset('cart_%s'%user.id,sku_id,count)

            # 有一种情况就是 该商品已经添加过了, hincrby 来进行累加
            # redis_conn.hincrby('cart_%s'%user.id,sku_id,count)
            #
            # # set
            # # 如果用户选中了,就添加到set中
            # if selected:
            #     redis_conn.sadd('cart_selected_%s'%user.id,sku_id)


            # I, 先通过redis链接实例创建管道
            pl = redis_conn.pipeline()

            # II 将指令通过管道区执行
            pl.hincrby('cart_%s' % user.id, sku_id, count)

            if selected:
                pl.sadd('cart_selected_%s'%user.id,sku_id)

            # III 让管道区执行
            pl.execute()

            # 4.3 返回响应
            return Response(serialzier.data)

        else:
            # 5. 未登录用户保存在cookie中
            # 5.1 先读取cookie,根据读取的信息进行判断
            cookie_cart = request.COOKIES.get('cart')
            if cookie_cart is not None:
                #说明有cookie数据
                # 解码
                decode = base64.b64decode(cookie_cart)
                cart = pickle.loads(decode)
            else:
                #说明没有数据
                cart = {}

            # cart = {1:{'count":3,'selected':True},2:{'count":1,'selected':False}}
            # sku_id:{count:xxx,selected:xxx}
            # 5.2 对数据进行操作
            # 如果 sku_id 存在 ,则 进行数据的累加
            # 如果 sku_id 不存在 ,则添加到cart中
            if sku_id in cart:
                #说明存在
                #进行数据的累加
                orginal_count = cart[sku_id]['count']
                count += orginal_count

            # 字典有这个字段就更新数据
            # 没有这个字段就添加数据
            cart[sku_id] = {
                'count': count,
                'selected': selected
            }

            #5.3 返回响应
            response = Response(serialzier.data)

            # 需要对 cart进行编码
            d = pickle.dumps(cart)
            cookie_str = base64.b64encode(d).decode()

            response.set_cookie('cart',cookie_str,24*3600)

            return response




#  Redis ,因为是存储在内存中,所以用尽量少的空间实现功能
#sku_id, count,selected

#string
# cart_1_sku_id:1
# cart_1_count:3
# cart_1_selected:true

#hash
# hash_key: key:value
# cart_1:  sku_id:count


#  cart_1:  1:3
#           2: 1

# 单独用一个集合区记录 已经勾选的id
# set
# [1,2]


# cookie
# { sku_id: {'count':xxx,'selected':True}, sku_id: {'count':xxx,'selected':True},}



# 1G    = 1024M
# 1M    =1024KB
#1KB    =1024B
# 1B    = 8byte
# byte 是计算机的最小单位 它只能是 0 或者 1

# A     0010    0001
# AAA
# 001000010010000100100001

# 001000    010010      000100      100001
# abcd

    def get(self,request):
        """
        我们有sku_id, count,selected 需要获取 商品的详细信息 和 count ,selected

        # 1.获取用户的登录状态
        # 2.登录用户从redis中获取sku_id, count,selected  (redis的数据结构)
        # 3.未登录用户从cookie中获取sku_id, count,selected (cookie的数据结构不一致)
        # 4.根据id查询模型商品的详细信息(模型)
        # 5.需要将模型转换为JSON
        """

        # 1.获取用户的登录状态
        try:
            user = request.user
        except Exception:
            user = None

        # redis 数据和 cookie数据 结构不一致
        # redis --> cookie数据  V 选择这个
        # cookie --> redis数据
        # {sku_id:{count:xxxx,selected:xxx}}


        # request.user.is_authenticated
        if user is not None and user.is_authenticated:
            # 2.登录用户从redis中获取sku_id, count,selected  (redis的数据结构)
            # 2.1 连接redis
            redis_conn = get_redis_connection('cart')
            # 2.2 获取redis数据,redis的数据是 bytes类型
            redis_cart = redis_conn.hgetall('cart_%s'%user.id)
            # [sku_id:count,1:2]
            redis_selected_ids = redis_conn.smembers('cart_selected_%s'%user.id)
            # [sku_id,1,2]

            # 2.3 将redis转换为 cookie结构的
            # {sku_id:{count:xxxx,selected:xxx}}
            cart = {}
            for sku_id,count in redis_cart.items():

                if sku_id in redis_selected_ids:
                    selected=True
                else:
                    selected=False

                cart[sku_id.decode()] = {
                    'count':int(count),
                    'selected': selected
                    # 'selected': sku_id in redis_selected_ids
                }

        else:
            # 3.未登录用户从cookie中获取sku_id, count,selected (cookie的数据结构不一致)

            cookie_cart = request.COOKIES.get('cart')

            if cookie_cart is not None:

                cart = pickle.loads(base64.b64decode(cookie_cart))
            else:
                cart = {}


        # # {sku_id:{count:xxxx,selected:xxx},sku_id:{count:xxxx,selected:xxx}}
        # 4.根据id查询模型商品的详细信息(模型)
        ids = cart.keys()
        # [1,2,3,4,5]
        skus = SKU.objects.filter(pk__in=ids)
        # skus = [SKU,SKU]
        # SKU 没有 count和select的值
        for sku in skus:
            sku.count = cart[str(sku.id)]['count']
            sku.selected = cart[str(sku.id)]['selected']
        # 5.需要将模型转换为JSON
        serializer = CartSKUSerializer(skus,many=True)

        return Response(serializer.data)

    def put(self,request):
        """
        # 1. 需要将 sku_id,count,selected 发送给后台
        # 2. 校验数据,并获取校验之后的数据
        # 3. 获取用户信息,根据用户信息进行判断
        # 4. 登录用户修改redis数据
        # 5. 未登录用户修改cookie数据
        """
        # 1. 需要将 sku_id,count,selected 发送给后台
        # 2. 校验数据,并获取校验之后的数据
        serializer = CartSerialzier(data=request.data)
        serializer.is_valid(raise_exception=True)

        sku_id = serializer.data.get('sku_id')
        count = serializer.data.get('count')
        selected = serializer.data.get('selected')
        # 3. 获取用户信息,根据用户信息进行判断
        try:
            user = request.user
        except Exception:
            user = None

        # request.user.is_authenticated
        if user is not None and user.is_authenticated:

            # 4. 登录用户修改redis数据
            # 4.1 连接redis
            redis_conn = get_redis_connection('cart')
            # 4.2 更新数据
            # 幂等 , 最终结果提交过来
            redis_conn.hset('cart_%s'%user.id,sku_id,count)
            if selected:
                redis_conn.sadd('cart_selected_%s'%user.id,sku_id)
            else:
                # 修改的话 如果没有选中则需要删除
                redis_conn.srem('cart_selected_%s'%user.id,sku_id)
            # 4.3 返回结果
            # 注意: 因为我们采用的是 幂等,必须要将服务器的数据返回给前段
            return Response(serializer.data)

        else:
            # 5. 未登录用户修改cookie数据

            # 5.1 获取cookie数据并且进行判断
            cookie_cart = request.COOKIES.get('cart')
            if cookie_cart is not None:
                cart = pickle.loads(base64.b64decode(cookie_cart))
            else:
                cart = {}
            # 5.2 更新数据
            cart[sku_id] = {
                'count':count,
                'selected':selected
            }

            # 5.3 返回响应
            response = Response(serializer.data)

            cookie_str = base64.b64encode(pickle.dumps(cart)).decode()
            response.set_cookie('cart',cookie_str,24*3600)

            return response



#     服务器 有 10个商品   11

# 非幂等 + 1       +   1



#     服务器 有 10个商品               15      20
# 幂等        把商品数据告诉服务器      15      20


    def delete(self,request):

        """
        # 1. 将删除的商品的id发送给后台
        # 2. 对id进行校验
        # 3. 获取用户登录状态,根据状态进行判断
        # 4. 登录用户操作redis
        # 5.未登录用户操作cookie
        """
        # 1. 将删除的商品的id发送给后台
        # 2. 对id进行校验
        serialier = CartDeleteSerailzier(data=request.data)
        serialier.is_valid(raise_exception=True)

        sku_id = serialier.validated_data.get('sku_id')
        # 3. 获取用户登录状态,根据状态进行判断
        try:
            user = request.user
        except Exception:
            user = None
        # request.user.is_authenticated
        if user is not None and user.is_authenticated:

            # 4. 登录用户操作redis
            #4.1 连接redis
            redis_conn = get_redis_connection('cart')
            #4.2 删除数据
            redis_conn.hdel('cart_%s'%user.id,sku_id)

            redis_conn.srem('cart_selected_%s'%user.id,sku_id)
            #4.3 返回响应
            return Response(status=status.HTTP_204_NO_CONTENT)

        else:
            # 5.未登录用户操作cookie

            # 5.1 获取cookie数据,并对cookie数据进行判断
            cookie_cart = request.COOKIES.get('cart')
            if cookie_cart is not None:
                cart = pickle.loads(base64.b64decode(cookie_cart))
            else:
                cart = {}
            # 5.2 删除数据

            if sku_id in cart:

                del cart[sku_id]


            # 5.3 返回响应
            response = Response(status=status.HTTP_204_NO_CONTENT)

            cookie_str = base64.b64encode(pickle.dumps(cart)).decode()
            response.set_cookie('cart',cookie_str,24*3600)

            return response

