#coding:utf8

import base64
import pickle
from django_redis import get_redis_connection


def merge_cookie_to_redis(request,user,response):
    """
    将cookie数据 合并到redis中

    cookie如何合并到reids中?? 原则是什么?
    1. 以cookie为准,redis数据删除
     2. 以redis数据为主, 如果cookie中有商品id信息,redis中没有,则将cookie的信息 添加到redis中
                如果cookie中的商品id,redis中也有, 以cookie的数量为准

    cookie: {sku_id:{'count':xxx,'selected':xxx}}
    cookes: {1:{count:10,selected:True},3:{count:30,selected:True},}

    redis: hash {sku_id:count}
          set [sku_id,sku_id]

    redis: {2:100,3:5}
        set: [2,3]


    最终的redis数据
    redis: {2:100,3:30,1:10}
            [2,3,1]
    # 1. 获取cookie数据
    # 2. 获取redis数据
    # 3. 对cookie数据进行遍历,添加到 新redis数据中
    # 4. 将新redis数据 添加到redis中

    2x+3y=15  x,y都必须是整数
    0   5
    1
    2
    3   3

    """
    # 1. 获取cookie数据
    cookie_str = request.COOKIES.get('cart')

    if cookie_str is not None:
        cookie_cart = pickle.loads(base64.b64decode(cookie_str))
        # 2. 获取redis数据

        # 2.1 连接redis
        redis_conn = get_redis_connection('cart')
        # 2.2 获取redis数据
        redis_cart = redis_conn.hgetall('cart_%s'%user.id)
        # {b'sku_id':b'count'}

        redis_selected_ids = redis_conn.smembers('cart_selected_%s'%user.id)
        # [b'sku_id',b'sku_id']

        # 2.3 需要对redis数据进行转换
        cart = {}
        for sku_id,count in redis_cart.items():
            cart[sku_id.decode()] = int(count)

        # 记录被选中id
        ids_list = []
        for sku_id in redis_selected_ids:
            ids_list.append(int(sku_id))


        # 3. 对cookie数据进行遍历,添加到 新redis数据中
        # cookes: {1:{count:10,selected:True},3:{count:30,selected:True},}
        for sku_id,count_selected_dict in cookie_cart.items():
            # 如果cookie中有商品id信息, redis中没有, 则将cookie的信息添加到redis中
            cart[sku_id] = count_selected_dict['count']
            # 如果cookie中的商品id, redis中也有, 以cookie的数量为准


            # 如果sku_id 的选中状态为 true,需要将sku_id 添加到 ids_list
            if count_selected_dict['selected']:
                ids_list.append(sku_id)

        # 4. 将新redis数据 添加到redis中
        # cart: {2: 100, 3: 30,1:10}
        # ids_list: [2, 3]

        redis_conn.hmset('cart_%s'%user.id,cart)

        # list: [2, 3]
        # *list

        #cart: {2: 100, 3: 30,1:10}
        # **cart
        redis_conn.sadd('cart_selected_%s'%user.id,*ids_list)

        # 5. cookie数据合并完了之后,应该删除
        # response.delete_cookie('cart')

        return response
    else:
        return response



