#coding:utf8
from rest_framework import serializers
from goods.models import SKU
class CartSerialzier(serializers.Serializer):

    sku_id = serializers.CharField(label='sku_id',required=True)
    count = serializers.IntegerField(label='数量',required=True,min_value=1)
    selected = serializers.BooleanField(label='是否选择',default=True,required=False)


    def validate(self, attrs):

        # 1. 商品id是否正确
        try:
            sku = SKU.objects.get(pk=attrs['sku_id'])
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')


        # 2. 商品个数是否超过库存
        if sku.stock < attrs['count']:
            raise serializers.ValidationError('库存不足')

        return attrs


from goods.models import SKU

class CartSKUSerializer(serializers.ModelSerializer):

    count = serializers.IntegerField(label='数量')
    selected = serializers.BooleanField(label='是否勾选')

    class Meta:
        model = SKU
        fields = ('id','count', 'name', 'default_image_url', 'price', 'selected')


class CartDeleteSerailzier(serializers.Serializer):

    sku_id = serializers.CharField(label='id',required=True)


    def validate(self, attrs):

        try:
            SKU.objects.get(pk=attrs['sku_id'])
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')

        return attrs