from django.shortcuts import render
from libs.captcha.captcha import captcha
from django_redis import get_redis_connection
from django.http import HttpResponse
from rest_framework.views import APIView
from . import constants
from rest_framework.generics import GenericAPIView
from .serializers import RegisterSmscodeSerializer
from random import randint
from rest_framework.response import Response
from rest_framework import status
from libs.yuntongxun.sms import CCP

# Create your views here.

# 图片验证 和短信验证
#APIView
#GenericAPIView
#ListAPIview,CreateAPIView

class RegisterImagecodeView(APIView):

    """
    GET     /verifications/imagecodes/(?P<image_code_id>.+)/

    GET     /verifications/imagecodes/?uuid=xxxxx
    """


    def get(self,request,image_code_id):

        # 1. 生成图片验证码
        text,image = captcha.generate_captcha()
        # 2. 记录在redis中

        # 2.1 连接redis
        redis_conn = get_redis_connection('code')
        # 2.2 把 验证码保存在redis中
        redis_conn.setex('img_%s'%image_code_id,constants.IMAGE_EXPIRE_TIME,text)

        # 3. 返回图片验证码
        # content_type 是一个MIME 类型
        # text/html  appplication/json
        # return  HttpResponse(image,content_type='text/html')
        return  HttpResponse(image,content_type='image/jpeg')


# APIView
# GenericAPIView
# ListAPIview,CreateAPIView
class RegisterSmscodeView(GenericAPIView):

    """
    GET     /verifications/smscodes/mobile/text/uuid/

    GET     /verifications/smscodes/?mobile=xxx&text=xxx&uuid=xxxx



    GET     /verifications/smscodes/(?P<mobile>1[345789]\d{9})/?text=xxxx & image_code_id=xxxx


    # 1. 手机号,图片验证码 和 uuid都要传递过来
    # 2.  对用户输入的 图片验证码进行校验
    # 3.  生成短信
    # 4. 发送短信

    """

    serializer_class = RegisterSmscodeSerializer

    def get(self,request,mobile):
        # 1. 手机号,图片验证码 和 uuid都要传递过来
        params = request.query_params

        # 2.  对用户输入的 图片验证码进行校验
        serializer = self.get_serializer(data=params)
        serializer.is_valid(raise_exception=True)

        # 3.  生成短信

        sms_code = '%06d'%randint(0,999999)

        # 3.1 将短信记录在redis中
        redis_conn = get_redis_connection('code')

        # 3.2 需要先判断 有没有给用户发送过
        flag = redis_conn.get('sms_flag_%s'%mobile)
        if flag:
            return Response(status=status.HTTP_429_TOO_MANY_REQUESTS)

        redis_conn.setex('sms_%s'%mobile,60*5,sms_code)

        # 3.3 记录一个标记,
        redis_conn.setex('sms_flag_%s'%mobile,60,1)


        # 4. 发送短信

        # ccp = CCP()
        # ccp.send_template_sms(mobile,[sms_code,5],1)

        from celery_tasks.sms.tasks import send_sms_code

        # 方法调用的通过delay
        # 要给函数传递的参数 是通过 delay 来实现传递的
        #send_sms_code(mobile,sms_code)     错误的
        send_sms_code.delay(mobile,sms_code)

        return Response({'message':'ok'})


