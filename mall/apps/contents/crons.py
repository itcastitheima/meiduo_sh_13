#coding:utf8
#所谓的静态化技术 其实就是 让用户访问的时候,访问我们提前准备好的 html
from goods.models import GoodsCategory,GoodsChannel
from contents.models import ContentCategory
from collections import OrderedDict
from django.template import loader
import os
import time

def generate_static_index_html():
    print('%s:generate_static_index' % time.ctime())

    # 数据查询出来
    # 使用有序字典保存类别的顺序
    # categories = {
    #     1: { # 组1
    #         'channels': [{'id':, 'name':, 'url':},{}, {}...],
    #         'sub_cats': [{'id':, 'name':, 'sub_cats':[{},{}]}, {}, {}, ..]
    #     },
    #     2: { # 组2
    #
    #     }
    # }
    # 初始化存储容器
    categories = OrderedDict()
    # 获取一级分类
    channels = GoodsChannel.objects.order_by('group_id', 'sequence')

    # 对一级分类进行遍历
    for channel in channels:
        # 获取group_id
        group_id = channel.group_id
        # 判断group_id 是否在存储容器,如果不在就初始化
        if group_id not in categories:
            categories[group_id] = {
                'channels': [],
                'sub_cats': []
            }

        one = channel.category
        # 为channels填充数据
        categories[group_id]['channels'].append({
            'id': one.id,
            'name': one.name,
            'url': channel.url
        })
        # 为sub_cats填充数据
        for two in one.goodscategory_set.all():
            # 初始化 容器
            two.sub_cats = []
            # 遍历获取
            for three in two.goodscategory_set.all():
                two.sub_cats.append(three)

            # 组织数据
            categories[group_id]['sub_cats'].append(two)

    # 广告和首页数据
    contents = {}
    content_categories = ContentCategory.objects.all()
    # content_categories = [{'name':xx , 'key': 'index_new'}, {}, {}]

    # {
    #    'index_new': [] ,
    #    'index_lbt': []
    # }
    for cat in content_categories:
        contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')

    # 进行模板的渲染
    context = {
        'categories': categories,
        'contents': contents
    }


    # 1. 获取模板
    # get_template 可以从模板中获取指定文件
    template = loader.get_template('index.html')

    #.2将数据传递给模板,让模板渲染数据
    #html_data 就是已经渲染好的 html
    html_data = template.render(context)

    #3. 将html写入指定的文件
    from django.conf import settings

    index_path = os.path.join(settings.FRONT_PATH,'index.html')

    with open(index_path,'w') as f:
        f.write(html_data)