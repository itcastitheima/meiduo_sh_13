#coding:utf8
# 当我们在进行登录的时候,我们的jwt会在认证完成之后,调用这个方法, 其中 user就是已经认证的信息
def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'username':user.username,
        'user_id':user.id
    }

import re
from users.models import User

# 我们根据用户输入的信息,来区分手机号和用户名  来获取用户的信息 ,
# 手机号是一个 能够满足正则需求的一个式子

# account 是用户在 登录的时候输入的 手机号或者用户名
def get_user_by_account(account):

    try:
        if re.match(r'1[3-9]\d{9}',account):
            #手机号
            user = User.objects.get(mobile=account)
        else:
            #用户名
            user = User.objects.get(username=account)
    except User.DoesNotExist:
        user = None

    return user






# 然后再检测用户输入的密码是否正确

from django.contrib.auth.backends import ModelBackend

# 我们继承自 ModelBackend 重写 它的认证方法
class UsernameMobileAuth(ModelBackend):

    # username=None, password=None
    # 登录的时候 用户输入的用户名和密码
    def authenticate(self, request, username=None, password=None, **kwargs):

        user = get_user_by_account(username)
        #检测用户输入的密码是否正确
        if user is not None and user.check_password(password):
            # user不是none 而且 密码校验正确 就返回user就可以
            return user

        return None



class SettingsBackend(object):
    """
    继承自object 需要实现 以下2个方法
    """

    def authenticate(self, request, username=None, password=None):
        user = get_user_by_account(username)
        if user is not None and user.check_password(password):
            return user

        return None

    # 如果继承自ModelBackend 不需要实现这个方法
    # 因为 ModelBackend 已经实现这个方法了
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None

