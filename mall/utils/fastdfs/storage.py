#coding:utf8

#1.您的自定义存储系统必须是以下子类 django.core.files.storage.Storage
from django.core.files.storage import Storage
from django.conf import settings
from fdfs_client.client import Fdfs_client
from django.utils.deconstruct import deconstructible
#4.就 可以使用 django.utils.deconstruct.deconstructible
# 类装饰器（这就是Django在FileSystemStorage上使用的）

@deconstructible
class FastDFSStorage(Storage):

    #2.Django必须能够在没有任何参数的情况下实例化您的存储系统。
    # 这意味着任何设置都应该来自django.conf.settings

    # def __init__(self,num=None):
    #
    #     if num is None:
    #         num = settings.NUM

    def __init__(self,ip=None,config_path=None):

        if ip is None:
            ip = settings.FDFS_URL
        self.ip = ip

        if config_path is None:
            config_path = settings.DEFAULT_FILE_STORAGE

        self.config_path = config_path



    #3.您的存储类必须实现_open()和_save() 方法
    # 以及适用于您的存储类的任何其他方法

    # 打开
    #因为我们是采用的 FastDFS实现资源的获取
    # 我们不需要打开
    # 这个方法 不需要实现
    def _open(self, name, mode='rb'):
        pass

    # 保存方法
    def _save(self, name, content, max_length=None):

        # name 只是文件的名字 不是完整的路径
        # content 内容 我们可以通过 content.read() 来获取上传资源的二进制数据

        #1. 创建客户端,并配置客户端文件
        # client = Fdfs_client('utils/fastdfs/client.conf')
        client = Fdfs_client(self.config_path)
        #2. 上传文件
        file_data = content.read() #文件二进制

        # buffer 上传二进制资源
        result = client.upload_by_buffer(file_data) # buffer
        #3. 获取 上传之后返回的数据,判断数据
        # result 就是上传之后返回的数据
        #{'Status': 'Upload successed.',
        # 'Group name': 'group1',
        # 'Uploaded size': '31.00KB',
        # 'Storage IP': '192.168.229.133',
        # 'Remote file_id': 'group1/M00/00/02/wKjlhVtn8vaAZFO1AAB9AY9TCZE833.jpg',
        # 'Local file name': '/home/python/Desktop/images/3.jpg'}

        if result.get('Status') == 'Upload successed.':
            #上传成功了
            return result.get('Remote file_id')
        else:
            raise Exception('上传失败')

    # 判断是否存在
    # 因为fastdfs已经做好了重名的解决方案,
    #你上传重复的文件我都可以保存
    def exists(self, name):

        return False

    # 获取资源的url,拼接 tracker_ip:port
    def url(self, name):

        # 默认只是把 name返回回去
        #name其实就是 group1/M00/00/02/wKjlhVtn_oeAI_n3AAT-6_mSZR0961.jpg

        # return 'http://192.168.229.133:8888/' + name
        return self.ip + name