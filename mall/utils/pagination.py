#coding:utf8
from rest_framework.pagination import PageNumberPagination

class CustomPageNumberPagination(PageNumberPagination):

    # http://api.example.org/accounts/?page=4&page_size=100
    page_size_query_param = 'page_size'  # 默认是page_size

    page_size = 4   #每页返回多少条

    max_page_size = 20  #最大返回多少条